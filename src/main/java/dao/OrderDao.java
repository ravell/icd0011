package dao;

import models.Order;
import models.OrderRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderDao {

    private static JdbcTemplate template;

    public final void setTemplate(JdbcTemplate template){
        this.template = template;
    }

    @Autowired
    public OrderDao(JdbcTemplate template){
        setTemplate(template);
    }

    public static Order insertOrder(Order order){

        String sql = "insert into salesOrder (id, orderNumber) " +
                "values (nextval('seq_order'), ?)";

        template.update(sql, order.getOrderNumber());
        if(order.getOrderRows() != null){

            String sql2 = "Insert into orderRow(id, ItemName, quantity, price, salesorder_id)" +
                    "values (nextval('seq_order'), ?, ?, ?,(select id FROM salesorder where ordernumber = '"
                    + order.getOrderNumber() +"'ORDER BY id DESC LIMIT 1))";

            List<OrderRow> rows = order.getOrderRows();

            for(OrderRow row : rows){
                template.update(sql2, row.getItemName(), row.getQuantity(), row.getPrice());
            }
        }
        String sql4 = "SELECT * FROM salesOrder WHERE  orderNumber = '" + order.getOrderNumber() + "'";
        List<Order> orders = template.query(sql4, new BeanPropertyRowMapper<>(Order.class));

        for(Order o : orders){
           order.setId(o.getId());
        }
        return order;
    }

    public static Object findOrderById(Long longId) {
        Order order = new Order();
        int id = Math.toIntExact(longId);

        String sql = "select salesOrder.id, salesOrder.orderNumber" +
                " from salesOrder WHERE salesOrder.id = " + id;
        List<Order> orders = template.query(sql, new BeanPropertyRowMapper<>(Order.class));
        for(Order o : orders){
            order.setId(o.getId());
            order.setOrderNumber(o.getOrderNumber());
        }
        String sql2 = "Select orderRow.id, orderRow.itemName, orderRow.quantity, orderRow.price from orderRow " +
                "Where orderRow.salesOrder_id = " + id;
        List<OrderRow> orderRows = template.query(sql2, new BeanPropertyRowMapper<>(OrderRow.class));
        for(OrderRow o : orderRows){
            order.add(o);
        }
        return order;
    }

    public static List<Order> getOrders(){
        String sql = "select Salesorder.id, salesOrder.orderNumber, o.itemName, o.quantity, o.price" +
                " from salesOrder left join orderrow o on salesorder.id = o.salesorder_id";
        return template.query(sql, new BeanPropertyRowMapper<>(Order.class));
    }

    public static void deleteOrder(Long longId) {
        //TODO: Implement delete order by ID
    }


}
