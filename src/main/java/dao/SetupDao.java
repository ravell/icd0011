package dao;

import models.ValidationError;
import models.ValidationErrors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import utility.FileUtil;

import java.util.ArrayList;
import java.util.List;

@Repository
public class SetupDao {

    @Autowired
    private JdbcTemplate template;

    @Autowired
    private ValidationErrors errors; //Not sure if needed?

    public void createSchema() {
        String schema = FileUtil.readFileFromClassPath("schema.sql");
        template.update(schema);
    }

    public void setupErrors(){
        List<ValidationError> eList = new ArrayList<>();
        List<String> arguments = new ArrayList<>();
        arguments.add("\"too_short_number\"");
        ValidationError error = new ValidationError();
        error.setCode("400");
        error.setArguments(arguments);
        eList.add(error);
        errors.setErrors(eList);
    }


}
