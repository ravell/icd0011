package models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Order {

    private Long id;

    private String orderNumber;

    public List<OrderRow> orderRows;

    public Order(String str) {
        this.id = id;
        this.orderNumber = str;
    }

    public Order (Long id, String str, List<OrderRow> orderRow){
        this.id = id;
        this.orderNumber = str;
        this.orderRows = orderRow;
    }

    public void add(OrderRow or) {
        if (orderRows == null) {
            orderRows = new ArrayList<>();
        }
        orderRows.add(or);
    }
    @Override
    public String toString() {
        return  "{" +
                "\"id\":\"" + id +'\"' +
                ", \"orderNumber\":\"" + orderNumber + '\"' +
                ", \"orderRows\":" + orderRows.toString() +
                "}";
    }
}

