package models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderRow {

    private String itemName;
    private Integer quantity;
    private Integer price;

    public OrderRow(String itemName, Integer quantity, Integer price){
        this.itemName = itemName;
        this.quantity = quantity;
        this.price = price;
    }
    @Override
    public String toString() {
        return '{' + "\"itemName\":" + '\"' + this.itemName + '\"' +
                ", \"quantity\":" + '\"' + this.quantity + '\"' +
                ", \"price\":" + '\"' + this.price + '\"' +
                '}';
    }
}
