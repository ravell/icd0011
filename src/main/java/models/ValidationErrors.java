package models;

import java.util.List;


public class ValidationErrors {

    public static List<ValidationError> errors;

    public static List<ValidationError> getErrors() { return errors; }

    public static String toJson(){
    return  "{ " +
            "\"errors\"" + ": " + errors +
            " }";
    }

    public void setErrors(List<ValidationError> errors) { this.errors = errors; }

}
