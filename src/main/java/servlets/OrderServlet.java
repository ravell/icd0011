package servlets;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import dao.OrderDao;
import models.Order;
import models.ValidationErrors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/orders")
public class OrderServlet extends HttpServlet {



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException{
        response.setContentType("application/json");
        String paramId = request.getParameter("id");
        if(paramId == null){
             new ObjectMapper().writeValue(response.getOutputStream(), OrderDao.getOrders());
        } else {
            Long longId = Long.valueOf(paramId);
            new ObjectMapper().writeValue(response.getOutputStream(), OrderDao.findOrderById(longId));
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        response.setContentType("application/Json");
        Order order = new ObjectMapper().readValue(request.getInputStream(), Order.class);
        String orderNumber = order.getOrderNumber();
        if(orderNumber.length() < 2 ){
            response.setStatus(400);
            JsonNode node = new ObjectMapper().readTree(ValidationErrors.toJson());
            node.get("errors").asText();
            new ObjectMapper().writeValue(response.getOutputStream(), node);
        }
        else {
        new ObjectMapper().writeValue(response.getOutputStream(), OrderDao.insertOrder(order));
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        response.setContentType("application/json");
        String paramId = request.getParameter("id");

        if(paramId == null){
            response.getWriter().print("No Id present  - Full database purge not available!");
        } else {
            Long longId = Long.valueOf(paramId);
            OrderDao.deleteOrder(longId);
            response.getWriter().print("/api/orders - Delete request received - Removed order with id -" + paramId);
        }

    }
}
