package servlets;

import config.Config;
import dao.SetupDao;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import utility.DataSourceProvider;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class StartUpListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce){
        var ctx = new AnnotationConfigApplicationContext(Config.class);

        try (ctx) {
            ctx.getBean(SetupDao.class).createSchema();
            ctx.getBean(SetupDao.class).setupErrors();
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        DataSourceProvider.closePool();
    }

}
