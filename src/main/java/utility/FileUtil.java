package utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class FileUtil {

    public static String readFileFromClassPath(String pathOnClasspath){
        try(InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(pathOnClasspath)){
            if(is == null){
                throw new IllegalArgumentException("Can't load file: " + pathOnClasspath);
            }

            BufferedReader buffer = new BufferedReader(new InputStreamReader(is));
            return buffer.lines().collect(Collectors.joining("\n"));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
