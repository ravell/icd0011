DROP TABLE IF EXISTS orderRow;
DROP TABLE IF EXISTS salesOrder;
DROP SEQUENCE IF EXISTS seq_order;

CREATE SEQUENCE seq_order start WITH 1;

CREATE TABLE salesOrder (
  id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq_order'),
  orderNumber VARCHAR(255) NOT NULL
);

CREATE TABLE orderRow (
  id BIGINT NOT NULL PRIMARY KEY DEFAULT nextVal('seq_order'),
  itemName VARCHAR(255) NOT NULL,
  quantity INT NOT NULL,
  price INT NOT NULL,
  salesOrder_id BIGINT NOT NULL,
  FOREIGN KEY(salesOrder_id)
  REFERENCES salesOrder
  ON DELETE RESTRICT
);